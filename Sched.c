//Justin Dietrich, Zach Pedersen
//Prof. Citro
//CST-315
//29 March 2022

#include<pthread.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>

enum states {running, ready, waiting, idle};

struct process{
    //command to run process
    enum states state;
    int id;

};

struct process procs[5];
int procSize = 5;

void *runProcess(void *v)
{
    struct process *p =(struct process*)v;
    sigaction();
    while(1)
    {
        printf("Process %i is running\n", p->id);
        sleep(1);
        
    }
}

void *takeInput(void *vargp)
{
    //print process statuses when 'procs' is entered
    while(1)
    {
        char str[64];
        fgets(str, 64, stdin);

        if (strcmp(str, "procs"))
        {
            for (int i = 0; i < procSize; i++)
            {
                printf("Process %i: %i\n", procs[i].id, procs[i].state);
            }
            sleep(1);
        }
    }
}

void *schedule(void *vargp)
{
	while (1) {
    struct process p = procs[0];
    if (p.state == idle)
    {
        p.state = ready;
    }
    if (p.state == ready)
    {
        p.state = running;
        pthread_t run;
        pthread_create(&run, NULL, runProcess, &p);
        pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
        //pthread_join(run, NULL);
        sleep(5);
        pthread_kill(run);
        p.state = ready;

        for(int i = 1; i < procSize; i++)
        {
            procs[i-1] = procs[i];
        }
        procs[procSize] = p;
    }
    else if (p.state == waiting)
    {
        for(int i = procSize - 1; i > 0; i--)
        {
            procs[i-1] = procs[i];
        }
        procs[procSize] = p;
    }
}
}

int main()
{
    for (int i = 0; i < 5; i++)
    {
        struct process p;
        p.state = idle;
        p.id = i;
        procs[i] = p; 
    }

    pthread_t schedulerid;
    pthread_t inputid;

    //pthread_create(&schedulerid, NULL, schedule, NULL);
    //pthread_join(schedulerid, NULL);
    //pthread_create(&inputid, NULL, takeInput, NULL);
    //pthread_join(inputid, NULL);
  int pt1 = pthread_create(&schedulerid, NULL, schedule, NULL);
int pt2 = pthread_create(&inputid, NULL, takeInput, NULL);
printf("%d %d \n", pt1, pt2);
pthread_join(schedulerid, NULL);
pthread_join(inputid, NULL);
}