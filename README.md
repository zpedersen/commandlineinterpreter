Zach Pedersen + Justin Dietrich's Entry for the Command Line Interpreter project! Installation Steps:

Interpreter:
Install Linux VirtualBox
Install and configure Ubuntu virtual machine
Install G++ Packages on Ubuntu machine
Take 'Interp' file from Bitbucket repository
Create a 'Interp.c' file in Ubuntu and paste source code
Use terminal commands "gcc -pthread Interp.c -o Interp", "./Interp" to compile and run
Follow the prompts!

Batch File: 
Install batch file from repository
Use terminal command "touch batch.sh" to create the batch file
Use command "gedit batch.sh" and paste the source code
Return to the command line interpreter and use "./batch.sh" to execute!

Scheduler:
Download "Sched.c" file from this repository
Create a "Sched.c" file in Ubuntu and paste source code
Use terminal commands "gcc -pthread Sched.c -o Sched" to output Sched to a file
Use terminal commands "gcc -pthread Interp.c -o Interp", "./Interp" to compile and run Shell
While inside the shell, use command "./Sched" to execute scheduler
Type "procs" to view process statuses!

File System Manager:
Download "FileSystem.c" file from this repository
Create a "FileSystem.c" file in Ubuntu and paste source code
Use terminal commands "gcc -pthread FileSystem.c -o FileSystem" to output FileSystem to a file
Use terminal commands "gcc -pthread Interp.c -o Interp", "./Interp" to compile and run Shell
While inside the shell, use command "./FileSystem" to execute File System Manager

COMMANDS:
				makes new directory: mkdir directoryName
             	removes a directory if empty: rmdir directoryName
				removes a directory if not empty: rm -r directoryName
				move file or rename file: mv sourceFile destinationFile
				create file: touch filename.extension
				duplicate file or directory: cp source destination
				find file in directory tree: find . -name filename
				display directory tree (must install tree! sudo apt-get install tree): tree /start/node
				display information about a directory: ls or ls -l for more detail
				navigate through directory: cd .. or ~ or directoryName


