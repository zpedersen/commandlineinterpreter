//File System Manager
//Zach Pedersen, Justin Dietrich
//Prof. Citro
//CST-315

#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/wait.h>

//getline declarations
char *input = NULL;
	size_t capline = 0;
int i;
	char *token;
	char *array[256];

bool quit = false;
char dir[512];

// User prompts
void startDisplay(){
	printf("File System Manager\n");
	printf("Run a command by typing and pressing Enter\n");
    printf("View commands by typing and entering 'help'\n");
	printf("To exit, type 'quit'\n");
}

// Command Input Prompt
void inputPrompt(){
	getcwd(dir, sizeof(dir));
	printf("%s: ", dir);
}

// Function to trim white space from Adam Rosenfield StackOverflow
char *trim(char *str)
{
	char *end;

	// Trim leading space
	while(isspace((unsigned char)*str)) str++;

	if(*str == 0)  // All spaces?
		return str;

	// Trim trailing space
	end = str + strlen(str) - 1;
	while(end > str && isspace((unsigned char)*end)) end--;
	// Write new null terminator character
	end[1] = '\0';

	return str;
}

//Command Execution
//Creates new process, waits for termination
void execute2()
{
	int pid = fork();
		if (pid != 0) {
			int s;
			waitpid(-1, &s, 0);
			}

    else {
            //makes new directory: mkdir directoryName
            if (strncmp(trim(array[0]), "mkdir ", 6) == 0 || 
				//removes a directory if empty: rmdir directoryName
				//removes a directory if not empty: rm -r directoryName
				strncmp(trim(array[0]), "rmdir ", 6) == 0 ||
				strncmp(trim(array[0]), "rm -r ", 6) == 0 ||
				//move file or rename file: mv sourceFile destinationFile
				strncmp(trim(array[0]), "mv ", 3) == 0 || 
				//create file: touch filename.extension
				strncmp(trim(array[0]), "touch ", 6) == 0 || 
				//duplicate file or directory: cp source destination
				strncmp(trim(array[0]), "cp ", 3) == 0 ||
				//find file in directory tree: find . -name filename
				strncmp(trim(array[0]), "find ", 5) == 0 ||
				//display directory tree (must install tree! sudo apt-get install tree): tree /start/node
				strncmp(trim(array[0]), "tree ", 5) == 0 ||
				//display information about a directory: ls or ls -l for more detail
				strncmp(trim(array[0]), "ls", 2) == 0 
            )
            {
                if(system(trim(array[0])) == -1){
		            perror("Invalid command. Type 'help' to view commands.\n");
					exit(errno);
				}
				else {
					exit(0);
				}	
            }
			//navigate through directory
			else if (strncmp(trim(array[0]), "cd ", 3) == 0)
			{
				*array += 3;
				chdir(trim(array[0]));
				//exit(0);
			}
			else if (strncmp(trim(array[0]), "help", 4) == 0)
			{
				printf("makes new directory: mkdir directoryName\n");
             	printf("removes a directory if empty: rmdir directoryName\n");
				printf("removes a directory if not empty: rm -r directoryName\n");
				printf("move file or rename file: mv sourceFile destinationFile\n");
				printf("create file: touch filename.extension\n");
				printf("duplicate file or directory: cp source destination\n");
				printf("find file in directory tree: find . -name filename\n");
				printf("display directory tree (must install tree! sudo apt-get install tree): tree /start/node\n");
				printf("display information about a directory: ls or ls -l for more detail\n");
				printf("navigate through directory: cd .. or ~ or directoryName\n");
				exit(0);
			}
			else{
				exit(0);
			}	
	  }
}

//Divide into tokens
//Add tokens into array
void makeTokens(char *input){
	const char d[2] = ";\n";
	i = 0;
		token = strtok(input, d);
			while (token != NULL) { 
				array[i] = token;
				if (strcmp(array[0], "quit") == 0) {
					printf("File System Manager Terminated\n");
					quit = true;
					break;
					//exit(0);
					//return 0;
				}
				else{
					execute2();
					token = strtok(NULL, d);
				}
				
				
			}
		array[i] = NULL;
}

int main(){
	startDisplay();
		while(quit == false){
			inputPrompt(); //Displays prompt
			getline(&input, &capline, stdin); //Reads input
		if(strcmp(input,"\n")==0){ //Checks if input is empty
			perror("Enter a command ");
				continue;
				}
	    makeTokens(input);
		}
	return 0;
}
